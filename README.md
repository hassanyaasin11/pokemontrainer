# Pokemon Trainer

This is a demo app that anyone call `clone` and then play around with. The app has several views, use the navbar on the homepage to cycle through them. The website can be reached through this link: https://pokemon-hh-app.herokuapp.com/

## Install

```
git clone https://gitlab.com/hassanyaasin11/pokemontrainer.git
cd pokemontrainer
npm install
```

# Usage

## How to use website

User types in username in name field and continues by clicking on "Login" button.

Avilable pokemons will be displayed on the "Pokemon Catalogue Page", user "catches" each pokemon by clicking on "pokeball" icon on corner of a pokemon.

The catched pokemon will be shown on the "profile" page.

## NgTodos

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.0.6.

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Contributors 

[Hassan Ali Yaasin (@hassanyaasin11)](@hassanyaasin11)

[Hussein Mohamed (@huah1600.hmah)](@huah1600.hmah)

## Licence 

Copyright 2022, Noroff Accelerate AS
