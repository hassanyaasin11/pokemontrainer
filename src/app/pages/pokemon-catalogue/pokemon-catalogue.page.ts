import { Component, OnInit } from '@angular/core';
import { PokemonDetail } from 'src/app/models/pokemon.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.css']
})
export class PokemonCataloguePage implements OnInit {

  get pokemons(): PokemonDetail[] {
    return this.pokemonCatalogueService.pokemons
  }

  get loading(): boolean {
return this.pokemonCatalogueService.loading /* ->pokemon-catalogue.page.html used there */
  }

  get error(): string {
    return this.pokemonCatalogueService.error
  }

    constructor(
    private readonly pokemonCatalogueService: PokemonCatalogueService
  ) { }

  ngOnInit(): void {
    this.pokemonCatalogueService.findAllPokemons()
  }

}
