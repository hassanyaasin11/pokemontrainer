import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { environment } from 'src/environments/environment';
import { Observable, tap } from 'rxjs';
import { Trainer } from '../models/trainer.model';
import { TrainerService } from './trainer.service';
import { PokemonDetail } from '../models/pokemon.model';

const { apiKey, apiTrainers } = environment


@Injectable({
  providedIn: 'root'
})
export class CatchService {

  constructor(
    private readonly http: HttpClient,
    private readonly pokemonService: PokemonCatalogueService, // All the currently available pokemons
    private readonly trainerService: TrainerService //Currently logged in trainer
  ) {}


  public addToCatchedPokemon(pokemonId: number): Observable<Trainer> {
    if(!this.trainerService.trainer) {
      throw new Error("addToFavourites: There is no trainer");
    }

    const trainer: Trainer = this.trainerService.trainer;

    const pokemon: PokemonDetail | undefined = this.pokemonService.pokemonById(pokemonId)

    if(!pokemon) {
      throw new Error("addToFavourites: No pokemon with id: " + pokemon)
    }

    // Does guitar exist in favourites.
    if(this.trainerService.isCatched(pokemonId)) {
      this.trainerService.removeCatchedPokemon(pokemonId)
    } 

    else {
      this.trainerService.addToCatchedPokemon(pokemon)  // Add pokemon to favourites
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey
    })

    //This is the values we want to update into the API
    return this.http.patch<Trainer>(`${apiTrainers}/${trainer.id}`, {
      pokemon: [...trainer.pokemon], // Already updated
    }, {
      headers
    })
    .pipe(
      tap((updatedTrainer: Trainer) => {
        this.trainerService.trainer = updatedTrainer
      })
    )
  }
}
