import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';

const { apiKey, apiTrainers } = environment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  // Dependency Injection.
  constructor(private readonly http: HttpClient) { }

  // Models, HttpCLient, Observables and RxJs operators.
  //Model: File describing the data we are working with

  //Observables have function to "pipe" function

  public login(username: string): Observable<Trainer> {
    return this.checkUsername(username)
      .pipe(
        switchMap((trainer: Trainer | undefined) => {
          if(trainer === undefined) { // user does not exist
            return this.createUser(username);
          }
          return of(trainer) ;
        })
      )
  }

  
  //Login

  //Check if user exists
  private checkUsername(username: string): Observable<Trainer | undefined> {
    return this.http.get<Trainer[]>(`${apiTrainers}?username=${username}`)
      .pipe(
        //RxJs Operators
        map((response: Trainer[]) => {
          return response.pop();
        })
      )
  }

  //IF NOT user - Create a user
  private createUser(username: string): Observable<Trainer> {
    // user
    const trainers = {
      username,
      pokemon: []
    }

    // headers -> API Key
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey
    });
    // POST - Create items on the server
    return this.http.post<Trainer>(apiTrainers, trainers, {
      headers
    })
  }

  //IF User || Created User -> Store user
}
