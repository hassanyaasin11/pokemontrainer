import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { PokemonDetail } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.utils';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {

  private _trainer?: Trainer;

  get trainer(): Trainer | undefined {
    return this._trainer;
  }

  set trainer(trainer: Trainer | undefined) {
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, trainer!) // Saves user in session storage
    this._trainer = trainer;
  }

  constructor() {
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer)
  }

  //To see if the pokemon is already "catched".
  public isCatched(pokemonId: number): boolean {
    if (this._trainer) {
      return Boolean(this.trainer?.pokemon.find((pokemon: PokemonDetail) => pokemon.id === pokemonId))
    }

    return false;
  }

  public addToCatchedPokemon(pokemon: PokemonDetail): void {
    if(this._trainer) {
    this._trainer.pokemon.push(pokemon)
    }
  }
  
  public removeCatchedPokemon(pokemonId: number): void {
    if (this._trainer) {
      this._trainer.pokemon = this._trainer.pokemon.filter((pokemon: PokemonDetail) => pokemon.id !== pokemonId)
    }
  }

}
