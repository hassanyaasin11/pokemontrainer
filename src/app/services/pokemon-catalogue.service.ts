import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, map } from 'rxjs';
import { environment } from 'src/environments/environment'
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon, PokemonDetail, Result } from '../models/pokemon.model';

import { StorageUtil } from '../utils/storage.utils';

const { apiPokemons } = environment
const { pokemonImages } = environment

@Injectable({
  providedIn: 'root'
})

export class PokemonCatalogueService {

  private _pokemons: PokemonDetail[] = [];
  private _error: string = "";
  private _loading: boolean = false;


  get pokemons (): PokemonDetail[] {
    return this._pokemons;  /* -> Because we want to expose our pokemon list to the component. */
  }

  get error(): string {
    return this._error;
  }

  get loading(): boolean {
    return this._loading
  }

  constructor(private readonly http: HttpClient) { }


  public findAllPokemons(): void {

    if (this._pokemons.length > 0 || this.loading) {
      return;
    }

    this._loading = true;
    this.http.get<Pokemon>(apiPokemons)
      .pipe(
        map((pokemon: Pokemon) => {
          let i: number = 0;
          pokemon.results.forEach((results: Result) => {
            this._pokemons[i] = { id: i+1, name: results.name, sprites: `${pokemonImages}/${i+1}.png` }
              i++
            });
            return this._pokemons
          }),

          finalize(() => {
            this._loading = false;
          })
        
      )
      .subscribe({
        next: response => {
          this._pokemons= response
          StorageUtil.storageSave(StorageKeys.Trainer, this._pokemons);
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      });
  }

  // get pokemons
  public pokemonById(id: number | any): PokemonDetail | undefined {
    return this._pokemons.find((pokemon: PokemonDetail) => pokemon.id === id);
  }

}