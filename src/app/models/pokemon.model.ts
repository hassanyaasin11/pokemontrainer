export interface Pokemon {
    count: string,
    next: string,
    previous?: string
    results: Result[]
}

export interface Result {
    name: string,
    url: string
}

export interface PokemonDetail {
    id: number
    name: string,
    sprites: string
}
