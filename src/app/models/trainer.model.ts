import { Pokemon, PokemonDetail } from "./pokemon.model";

export interface Trainer {
id: string,
username: string,
pokemon: PokemonDetail[]
}
