import { Component, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerService } from 'src/app/services/trainer.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  get trainer(): Trainer | undefined {
    return this.trainerService.trainer
  }
  constructor(
    private readonly trainerService: TrainerService
  ) {}

  ngOnInit(): void {
  }

  public logOut(): void {
    Swal.fire({

      title: 'Are you sure you want to logout?',

      icon: 'warning',

      showCancelButton: true,

      confirmButtonText: 'Yes, go ahead.',

      cancelButtonText: 'No, stay on page.',

    }).then((result) => {

      if (result.value) {

        sessionStorage.clear();

       

        window.location.reload();



        Swal.fire('You are logged out!', 'success');

      } else if (result.dismiss === Swal.DismissReason.cancel) {

        Swal.fire('Cancelled');

      }

    });
    
  }
}
