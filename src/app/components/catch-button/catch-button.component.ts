import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { CatchService } from 'src/app/services/catch.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-catch-button',
  templateUrl: './catch-button.component.html',
  styleUrls: ['./catch-button.component.css']
})
export class CatchButtonComponent implements OnInit {

  public loading: boolean = false

  public isCatched: boolean = false;

  @Input() guitarId: string = "";
  @Input() pokemonId: number = 0;

  constructor(
    private readonly trainerService: TrainerService,
    private readonly catchService: CatchService
  ) { }

  ngOnInit(): void {
    // Inputs are resolved!
    this.isCatched = this.trainerService.isCatched(this.pokemonId)
  }

  onCatchClick(): void {
    this.loading = true
    // Add the pokemon to the favourites
    this.catchService.addToCatchedPokemon(this.pokemonId)
      .subscribe({
        next: (trainer: Trainer) => {
          this.loading = false
          this.isCatched = this.trainerService.isCatched(this.pokemonId)
        },
        error: (error: HttpErrorResponse) => {
          console.log("ERROR", error.message);
        }
      })
  }

}
