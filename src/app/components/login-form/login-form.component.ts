import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Trainer } from 'src/app/models/trainer.model';
import { LoginService } from 'src/app/services/login.service';
import { TrainerService } from 'src/app/services/trainer.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {

  @Output() login: EventEmitter<void> =new EventEmitter()

  // Depenedency Injection (DI)
  constructor(
    private readonly loginService: LoginService,
    private readonly trainerService: TrainerService
    ) { }

    //After clicking on Log in button, username will be stored in sessionStorage.
  public loginSubmit(loginForm: NgForm): void {

    // username
    const { username } = loginForm.value

    if(username.length == 0)
      Swal.fire("You need a username")

    else {
    this.loginService.login(username)
      .subscribe({
        
        next: (trainer: Trainer) => {
          this.trainerService.trainer = trainer;
          this.login.emit()
        },
        error: () => {
          // Handle that locally.
        }
      })
  }}

}
